(asdf:defsystem #:angle-mask-fov
    :serial t
    :description "FOV using the angle mask technique."
    :author "Paul Donnelly"
    :components ((:file "package")
                 (:file "fov")))

