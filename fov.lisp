;; [[file:exposition.org::*FOV][FOV]]
(in-package #:angle-mask-fov)

(defvar *test-input*
  (make-array '(25 25)
              :initial-contents
              '((0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                (0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                (0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0)
                (0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0))
              :element-type 'bit))
(defvar *test-output* (make-array '(25 25) :element-type 'bit))

(declaim (ftype (function (single-float single-float)
                          (values single-float single-float))
                %cell-slope-extent)
         (inline %cell-slope-extent))
(defun %cell-slope-extent (dx dy)
  "Given a cell at a given x,y offset from the view point, return the slope range covered by a wall in that cell, located in its middle and going from edge to edge, parallel to the y axis. The range start is clamped at 0 inclusive and the range end at 1 exclusive, but make sure to give x and y in the first octant."
  (declare (optimize (speed 3)))
  ;; Think about the geometry: we are in the center of our cell, and on the x axis we are interested in the center position as well. But on the y axis, each cell actually extends to 0.5 units above and below its center.
  (values (max 0.0 (/ (- dy 0.5) dx))
          (min 0.999999 (/ (+ dy 0.5) dx))))

(defmacro |Initialize for octant| ()
  `(fill shadow-mask 0))

(defmacro |Visit for FOV| ()
  `(multiple-value-bind (start end) (%cell-slope-extent i j)
     (let* ((start-bit (truncate (the (single-float 0.0 1.0)
                                      (* start shadow-mask-size))))
            (end-bit (truncate (the (single-float 0.0 1.0)
                                    (* end shadow-mask-size))))
            (seen (loop for n from start-bit to end-bit
                     when (zerop (aref shadow-mask n))
                     do (return 1)
                     finally (return 0))))
       (when (plusp (aref obstructions x y))
         (fill shadow-mask 1 :start start-bit :end (1+ end-bit)))
       (setf (aref output x y) seen))))

(defmacro fov-octant ((dx dy) y-is-outer-loop)
  (assert (member dx '(-1 +1)))
  (assert (member dy '(-1 +1)))
  (let* ((vo (if y-is-outer-loop 'y 'x))
         (vi (if y-is-outer-loop 'x 'y))
         (vo0 (if y-is-outer-loop 'y0 'x0))
         (vi0 (if y-is-outer-loop 'x0 'y0))
         (x-lim (if (minusp dx) -1 '(array-dimension obstructions 0)))
         (y-lim (if (minusp dy) -1 '(array-dimension obstructions 1)))
         (vo-lim-form (if y-is-outer-loop y-lim x-lim))
         (vi-lim-form (if y-is-outer-loop x-lim y-lim))
         (dvo (if y-is-outer-loop dy dx))
         (dvi (if y-is-outer-loop dx dy)))
    `(let ((vo-lim ,vo-lim-form)
           (vi-lim ,vi-lim-form))
       (|Initialize for octant|)
       (do ((,vo (+ ,vo0 ,dvo) (+ ,vo ,dvo))
            (i 1.0 (1+ i)))
           ((= ,vo vo-lim))
         (declare (type single-float i)
                  (type fixnum ,vo))
         (do ((,vi ,vi0 (+ ,vi ,dvi))
              (j 0.0 (1+ j)))
             ((or (= ,vi vi-lim)
                  (< i j)))
           (declare (type single-float j)
                    (type fixnum ,vi))
           (|Visit for FOV|))))))

(declaim (ftype (function ((simple-array bit 2)
                           fixnum fixnum
                           (simple-array bit 2))
                          t)
                fov))

(defmacro fov-diagonal (dx dy)
  (assert (member dx '(-1 +1)))
  (assert (member dy '(-1 +1)))
  `(let* ((obstructed 1)
          (xdist (if (minusp ,dx) x0 (- (array-dimension obstructions 0) x0)))
          (ydist (if (minusp ,dy) y0 (- (array-dimension obstructions 1) y0)))
          (distance (min xdist ydist)))
     (loop for i fixnum from 1 below distance do
          (let ((x (+ x0 (* i ,dx)))
                (y (+ y0 (* i ,dy))))
            (setf (aref output x y) obstructed)
            (when (plusp (aref obstructions x y))
              (setf obstructed 0))))))

(defun fov (obstructions x0 y0 output)
  "Computes, for each cell and from a given (x0,y0) position, whether that cell is visible. OUTPUT should be a bit array, which will have visible cells set to 1 and other cells to 0. OBSTRUCTIONS must be a bit array of the same dimensions, where 1 indicates an obstructed cell and other cells are 0."
  (declare (optimize (speed 3)))
  (assert (equal (array-dimension obstructions 0)
                 (array-dimension output 0)))
  (assert (equal (array-dimension obstructions 1)
                 (array-dimension output 1)))
  (let* ((shadow-mask-size (* 2 (max (array-dimension obstructions 0)
                                     (array-dimension obstructions 1))))
         (shadow-mask
          ;; SBCL will only allocate the shadow mask with dynamic extent when it knows the array will be "small". Hence the assertion.
          (progn (assert (< shadow-mask-size 300))
                 (make-array shadow-mask-size 
                             :element-type 'bit))))
    (declare (type (simple-bit-vector *) shadow-mask)
             (type fixnum shadow-mask-size)
             (dynamic-extent shadow-mask))

    (fov-octant (+1 +1) nil)
    (fov-octant (+1 +1) t)

    (fov-octant (+1 -1) nil)
    (fov-octant (+1 -1) t)

    (fov-octant (-1 -1) nil)
    (fov-octant (-1 -1) t)

    (fov-octant (-1 +1) nil)
    (fov-octant (-1 +1) t)

    (fov-diagonal +1 -1)
    (fov-diagonal +1 +1)
    (fov-diagonal -1 +1)
    (fov-diagonal -1 -1)

    (setf (aref output x0 y0) 1))
  output)


(defun test-fov-speed (&optional (number-of-runs 1000))
  (format t "~a runs:~%" number-of-runs)
  (time
   (loop repeat number-of-runs do
        (fov *test-input* 10 10 *test-output*)))
  *test-output*)

(defun test-big-fov-speed (&optional (number-of-runs 3000))
  (format t "~a runs:~%" number-of-runs)
  (let ((*test-input* (make-array '(100 100)
                                  :element-type 'bit))
        (*test-output* (make-array '(100 100)
                                   :element-type 'bit)))
    (time
     (loop repeat number-of-runs do
          (fov *test-input* 50 50 *test-output*)))
    (aref *test-output* 0 0)))

(defun test-small-fov-speed (&optional (number-of-runs 200000))
  (format t "~a runs:~%" number-of-runs)
  (let ((*test-input* (make-array '(10 10)
                                  :element-type 'bit))
        (*test-output* (make-array '(10 10)
                                   :element-type 'bit)))
    (time
     (loop repeat number-of-runs do
          (fov *test-input* 5 5 *test-output*)))
    (aref *test-output* 0 0)))

(defun test-fov-from-everywhere ()
  (time
   (loop for x below (array-dimension *test-input* 0) do
        (loop for y below (array-dimension *test-input* 1) do
             (fov *test-input* x y *test-output*)
             (print *test-output*)
             (sleep 0.5)))))
